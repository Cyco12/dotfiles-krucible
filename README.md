# dotfiles
My dotfiles for various setups; Mostly just my desktop Manjaro+i3 home environment.

Different configs have their own branches.

Heavy inspiration taken from https://reddit.com/r/unixporn and [/u/ilovecookieee](https://github.com/manilarome/the-glorious-dotfiles)
